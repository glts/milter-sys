# milter-sys changelog

## 0.2.3 (2021-12-02)

*   Minor updates to project metadata.

## 0.2.2 (2021-04-30)

*   Properly specify minimal dependency versions in `Cargo.toml`.

## 0.2.1 (2020-12-02)

*   Remove hack in `build.rs` that was necessary to make the build work on
    docs.rs. This hack was used to work around missing pkg-config metadata, but
    that metadata has since been added to the docs.rs build environment.
*   Various minor updates to project metadata.

## 0.2.0 (2020-02-06)

Initial release.
