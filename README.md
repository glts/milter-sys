# milter-sys

The **milter-sys** library provides low-level Rust FFI bindings to libmilter,
the sendmail mail filter API.

## Requirements

This crate requires the milter C library (libmilter) to be available.

☞ *On Debian and Ubuntu, install the package `libmilter-dev`.*

## Usage

See the [milter crate] for idiomatic Rust bindings to the milter library.

[milter crate]: https://crates.io/crates/milter

## Licence

Copyright © 2019–2022 David Bürgin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
